package yangyd.authserver;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

/**
 * {@code @EnableResourceServer} is necessary for exposing the /user endpoint to clients with valid bearer token.
 */
@SpringBootApplication
@EnableAuthorizationServer
@EnableResourceServer
class Application { }
