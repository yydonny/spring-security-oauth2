package yangyd.authserver;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/")
public class UserController {
  @RequestMapping("/user")
  public Principal user(Principal user) {
    return user;
  }
}
