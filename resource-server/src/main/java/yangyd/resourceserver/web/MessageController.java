package yangyd.resourceserver.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import yangyd.resourceserver.model.Message;

@RestController
@RequestMapping("/")
public class MessageController {
  @RequestMapping("/message")
  public Message message() {
    return new Message("this is a pen");
  }
}
