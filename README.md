

### Overview

- `resource-server` is guarded by `auth-server`
- `user` wants to access its data in the `resource-server`, through a `client`
- the `client` direct the `user` to the `auth-server`, along with a client_id and a callback URI.
- the `user` login to the `auth-server`, and choose to authorize the `client` on an authorization form
- the `auth-server` send `user` back to the callback URI of the `client`, along with a temporary token.
- the `client` take the temporary token back to `auth-server`, authenticate with its client_secret
- an access_token is granted to the `client`.
- the `client` then use the access_token to access the `resource-server`. (the Authorization: Bearer header)
- the `resource-server` take the access_token to a special endpoint of `auth-server`, to verify its validity as well as get user identity and granted scopes. 
- Or the `resource-server` could check the token via some shared cache.

### OAuth2 Flow

1. GET http://localhost:26512/uaa/oauth/authorize with following query parameter

```
response_type=code
client_id=acme
redirect_uri=http://localhost:8080/authorized
```

It should be opened with a browser window. The user provide its username/password to login. (The login process is custom
implementation of the auth server, here it is simply the HTTP Basic login box presented by the browser).

Then a personalized form is displayed for user to review and authorize the client (`acme`) with certain scopes.

2. The form is submitted to POST http://localhost:26512/uaa/oauth/authorize with following parameter

```
user_oauth_approval:true
scope.scope1:true
scope.scope2:true
scope.scope3:true
```

3. Exchange token (Grant access)

The client redirect URI is called with `code=xxxxxxx` to pass a temporary token to the client. 


Then the client invoke `/oauth/token` at the authorization server to obtain the access token. Client must authenticate with its `client_id:client_secret`.
The redirect URI (just host?) must match the one specified in step 1. (normally client redirect URI is pre-registered rather than passed as form parameter to `/oauth/authorize`).

```
POST /uaa/oauth/token HTTP/1.1
Host: localhost:26512
Content-Type: application/x-www-form-urlencoded
Authorization: Basic YWNtZTphY21lc2VjcmV0
Cache-Control: no-cache

grant_type=authorization_code&client_id=acme&redirect_uri=http%3A%2F%2Fexample.com&code=BMmmO

```


Then the access token is granted:

```
{
    "access_token": "af293e46-af1a-49f0-bd64-1c207889cf2a",
    "expires_in": 43199,
    "refresh_token": "3eb19ee1-3b89-4127-97e2-ae86741d9b4f",
    "scope": "openid",
    "token_type": "bearer"
}
```


### Examples

**The OAuth2 client is demonstrated by the `ui-server`. Try access `/user`, `/resource/message` of the `ui-server`. (`auth-server` and `resource-server` must be up as well)**

[Authorization Page]

[Authorization Page]: http://localhost:10086/a/oauth/authorize?response_type=code&client_id=aim&redirect_uri=http://localhost:8080/authorized

```Powershell
# Token exchange
http --form -a aim:aimpass `
  post localhost:10086/a/oauth/token `
  'grant_type=authorization_code' 'client_id=aim' `
  'redirect_uri=http://localhost:8080/authorized' 'code=FSKuGX'

```


```Powershell
$token='5a877817-a15f-4718-aa0b-5f909797f10a'

# check user info
http localhost:10086/a/user "Authorization: Bearer $token"

# Access resource serve API
http localhost:8080/message "Authorization: Bearer $token"

```



