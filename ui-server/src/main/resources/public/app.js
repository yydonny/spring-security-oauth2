angular.module('hello', [ 'ngRoute' ])
  .config(function($routeProvider, $httpProvider) {

    $routeProvider.when('/', {
      templateUrl : 'home.html',
      controller : 'home',
      controllerAs: 'controller'
    })
    .otherwise('/');

    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
  })
  .controller('home', function($http) {
    var self = this;
    $http.get('/resource/message').then(function(response) {
      self.greeting = response.data;
    })
  })

  .controller('navigation', function($rootScope, $http, $location) {
    var self = this;

    var authenticate = function(credentials) {
      $http.get('user')
      .then(function(response) {
        if (response.data.name) {
          $rootScope.authenticated = true; // logged in
        } else {
          $rootScope.authenticated = false;
        }
      }, function() {
        $rootScope.authenticated = false;
      });
    };
    authenticate();

    // clear cached credentials on each navigation
    console.log('Clear credentials');
    self.credentials && console.log('(%s/%s)', self.credentials.username, self.credentials.password);
    self.credentials = {};

    self.logout = function() {
      $http.post('logout', {}).finally(function() {
        $rootScope.authenticated = false;
        $location.path("/");
      });
    };

  });
