package yangyd.uiserver.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping("/")
public class FooController {
  @RequestMapping("/foo")
  public Map<String, String> foo() {
    return Collections.singletonMap("message", "this is a pen");
  }
}
