package yangyd.uiserver.config;

import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.stereotype.Component;

@Configuration
class WebSecurityConfig {

  /**
   * (?)
   * If you want to make the UI application be able to refresh expired access tokens automatically, you have to get an
   * OAuth2RestOperations injected into the Zuul filter that does the relay. You can do this by just creating a bean
   * of that type (check the OAuth2TokenRelayFilter for details):
   */
  @Bean
  OAuth2RestTemplate OAuth2RestTemplate(OAuth2ProtectedResourceDetails resource, OAuth2ClientContext context) {
    return new OAuth2RestTemplate(resource, context);
  }

  /**
   * Spring Cloud OAuth2 client support
   *
   * {@code @EnableOAuth2Sso} will handle /login by sending a 302 Redirect to the configured ${security.oauth2.client.userAuthorizationUri},
   * with the following parameter:
   *   response_type=code
   *   client_id=
   *   redirect_uri=http://localhost:8080/login  <-- the callback url is also /login and handled by spring
   *
   * Basically this is still the classical session based access control, except that /login is outsourced to the auth-server.
   * And the access token granted is kept in the session.
   *
   * Also Zuul proxy is aware of the access token and will use it when relaying traffic (?)
   *
   */
  @EnableOAuth2Sso
  @Component
  static class O extends WebSecurityConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
      http.authorizeRequests()
          .antMatchers("/index.html", "/home.html", "/").permitAll()
          .anyRequest().authenticated()
          .and()
          .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
    }
  }
}
